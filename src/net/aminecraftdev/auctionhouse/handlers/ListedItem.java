package net.aminecraftdev.auctionhouse.handlers;

import net.aminecraftdev.auctionhouse.handlers.gui.ListHandler;
import net.aminecraftdev.auctionhouse.managers.AuctionDataManager;
import net.aminecraftdev.auctionhouse.utils.NumberUtils;
import net.aminecraftdev.auctionhouse.utils.core.Message;
import net.aminecraftdev.auctionhouse.utils.dependencies.VaultHelper;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 20-Sep-17
 */
public class ListedItem {

    private static AuctionDataManager auctionDataManager;

    private long expiryTime;
    private ItemStack itemStack;
    private UUID uuid;
    private OfflinePlayer merchant;
    private double price;

    public ListedItem(long expiryTime, ItemStack itemStack, OfflinePlayer merchant, double price) {
        this(expiryTime, itemStack, UUID.randomUUID(), merchant, price);
    }

    public ListedItem(long expiryTime, ItemStack itemStack, UUID uuid, OfflinePlayer merchant, double price) {
        this.uuid = uuid;
        this.itemStack = itemStack;
        this.expiryTime = expiryTime;
        this.merchant = merchant;
        this.price = price;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public long getExpiryTime() {
        return expiryTime;
    }

    public UUID getUuid() {
        return uuid;
    }

    public OfflinePlayer getMerchant() {
        return merchant;
    }

    public double getPrice() {
        return price;
    }

    public void expire() {
        remove();
        AuctionPlayer.getAuctionPlayer(merchant.getUniqueId()).decreaseCurrentListings();
        auctionDataManager.createExpiredItem(this);
    }

    public void adminCancel(Player player) {
        if(!player.hasPermission("ah.admin")) return;

        remove();
        Message.AdminRemoveListedItem.msg(player);
        AuctionPlayer.getAuctionPlayer(merchant.getUniqueId()).decreaseCurrentListings();

        new ListHandler(player).open();
    }

    public void cancel(Player player) {
        if(!player.getUniqueId().equals(merchant.getUniqueId())) return;

        remove();
        auctionDataManager.createExpiredItem(this);
        AuctionPlayer.getAuctionPlayer(merchant.getUniqueId()).decreaseCurrentListings();

        Message.PlayerRemoveListedItem.msg(player);
        new ListHandler(player).open();
    }

    public void purchase(Player player) {
        if(player.getUniqueId().equals(getMerchant().getUniqueId())) {
            Message.OwnItem.msg(player);
            return;
        }

        double balance = VaultHelper.getEconomy().getBalance(player);

        if(balance < getPrice()) {
            Message.INSUFFICIENT_FUNDS.msg(player);
            return;
        }

        if(player.getInventory().firstEmpty() == -1) {
            Message.INVENTORY_SPACE.msg(player);
            return;
        }

        remove();
        player.getInventory().addItem(getItemStack());
        VaultHelper.getEconomy().withdrawPlayer(player, getPrice());
        VaultHelper.getEconomy().depositPlayer(getMerchant(), getPrice());
        AuctionPlayer.getAuctionPlayer(merchant.getUniqueId()).decreaseCurrentListings();

        Message.PlayerBoughtListedItemSender.msg(player, NumberUtils.formatDouble(getPrice()));
        Message.MONEY_TAKEN.msg(player, NumberUtils.formatDouble(getPrice()));

        if(getMerchant().isOnline()) {
            Message.PlayerBoughtListedItemReceiver.msg(getMerchant().getPlayer(), NumberUtils.formatDouble(getPrice()));
            Message.MONEY_GIVEN.msg(getMerchant().getPlayer(), NumberUtils.formatDouble(getPrice()));
        }

        new ListHandler(player).open();
    }

    public void remove() {
        auctionDataManager.deleteItem(this);
    }

    public static void load(AuctionDataManager aucDataManager) {
        auctionDataManager = aucDataManager;
    }

}
