package net.aminecraftdev.auctionhouse.handlers;

import net.aminecraftdev.auctionhouse.AuctionHouse;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 20-Sep-17
 */
public class ItemListing {

    private static final AuctionHouse PLUGIN = AuctionHouse.getI();

    private static double defaultPrice, maxPrice, minPrice;
    private static int defaultListingHours, maxListingHours, minListingHours;

    private Player player;
    private ItemStack itemStack = null;
    private double price;
    private int listingHours;

    public ItemListing(Player player) {
        this.player = player;
        this.price = defaultPrice;
        this.listingHours = defaultListingHours;
    }

    public Player getPlayer() {
        return player;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public void setItemStack(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public double getPrice() {
        return price;
    }

    public boolean setPrice(double newPrice) {
        if(newPrice > maxPrice || newPrice < minPrice) return false;

        this.price = newPrice;
        return true;
    }

    public int getListingHours() {
        return this.listingHours;
    }

    public boolean increaseListingHours() {
        if(this.listingHours == maxListingHours) return false;

        this.listingHours++;
        return true;
    }

    public boolean decreaseListingHours() {
        if(this.listingHours == minListingHours) return false;

        this.listingHours--;
        return true;
    }

    public static void load() {
        defaultPrice = PLUGIN.getConfig().getDouble("Settings.defaultListingPrice");
        defaultListingHours = PLUGIN.getConfig().getInt("Settings.defaultListingHours");

        maxPrice = PLUGIN.getConfig().getDouble("Settings.Limits.maxSellPrice");
        maxListingHours = PLUGIN.getConfig().getInt("Settings.ListHours.maxHours");

        minPrice = PLUGIN.getConfig().getDouble("Settings.Limits.minSellPrice");
        minListingHours = PLUGIN.getConfig().getInt("Settings.ListHours.minHours");
    }

}
