package net.aminecraftdev.auctionhouse.handlers.data;

import net.aminecraftdev.auctionhouse.AuctionHouse;
import net.aminecraftdev.auctionhouse.handlers.DataManagement;
import net.aminecraftdev.auctionhouse.handlers.ExpiredItem;
import net.aminecraftdev.auctionhouse.handlers.ListedItem;
import net.aminecraftdev.auctionhouse.utils.Callback;
import net.aminecraftdev.auctionhouse.utils.reflection.ItemStackReflection;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 20-Sep-17
 */
public class DatabaseManagement implements DataManagement {

    private final AuctionHouse PLUGIN;

    public DatabaseManagement(AuctionHouse auctionHouse) {
        PLUGIN = auctionHouse;
    }

    @Override
    public void createTables() {
        List<String> itemsKeys = new ArrayList<>();
        List<String> expiredItemsKeys = new ArrayList<>();

        itemsKeys.add("UUID varchar(255) NOT NULL");
        itemsKeys.add("MERCHANT varchar(255) NOT NULL");
        itemsKeys.add("ITEM text NOT NULL");
        itemsKeys.add("PRICE bigint NOT NULL");
        itemsKeys.add("EXPIRED_TIME bigint NOT NULL");
        itemsKeys.add("PRIMARY KEY (uuid)");

        expiredItemsKeys.add("UUID varchar(255) NOT NULL");
        expiredItemsKeys.add("MERCHANT varchar(255) NOT NULL");
        expiredItemsKeys.add("ITEM text NOT NULL");
        expiredItemsKeys.add("EXPIRED_TIME bigint NOT NULL");
        expiredItemsKeys.add("WITHER_OUT_TIME bigint NOT NULL");
        expiredItemsKeys.add("PRIMARY KEY (uuid)");

        PLUGIN.getAbstractDatabase().createTable("items", null, itemsKeys); //Creates database table 'items' with the keys listed in List ItemKeys
        PLUGIN.getAbstractDatabase().createTable("expired_items", new LinkedList<>(), expiredItemsKeys); // Creates database table 'expired_items' with the keys listed in list expiredItemsKeys
    }

    @Override
    public void saveItem(ListedItem listedItem, Callback<ListedItem> callback) {
        String itemUUID = listedItem.getUuid().toString();
        String merchantUUID = listedItem.getMerchant().getUniqueId().toString();
        String item = ItemStackReflection.serialize(listedItem.getItemStack());
        double price = listedItem.getPrice();
        long expireTime = listedItem.getExpiryTime();

        String values = "('" + itemUUID + "', '" + merchantUUID + "', '" + item + "', " + price + ", " + expireTime + ")";
        String sql = "REPLACE INTO items(uuid, merchant, item, price, expired_time) VALUES " + values;

        PLUGIN.getLogger().log(Level.INFO, "Attempting to save item, " + listedItem.getUuid().toString() + " to SQL...");

        Bukkit.getScheduler().runTaskAsynchronously(PLUGIN, () -> {
            try {
                PLUGIN.getAbstractDatabase().updateSQL(sql);
                callback.call(listedItem);
            } catch (Exception ex) {
                ex.printStackTrace();
                callback.call(null);
            }
        });
    }

    @Override
    public void saveExpiredItem(ExpiredItem expiredItem, Callback<ExpiredItem> callback) {
        String itemUUID = expiredItem.getUuid().toString();
        String merchantUUID = expiredItem.getMerchant().getUniqueId().toString();
        String item = ItemStackReflection.serialize(expiredItem.getItemStack());
        long expiredTime = expiredItem.getExpiryTime();
        long witherOutTime = expiredItem.getWitherOutTime();
        String values = "('" + itemUUID + "', '" + merchantUUID + "', '" + item + "', " + expiredTime + ", " + witherOutTime + ")";
        String sql = "REPLACE INTO expired_items(uuid, merchant, item, expired_time, wither_out_time) VALUES " + values;

        Bukkit.getScheduler().runTaskAsynchronously(PLUGIN, () -> {
            try {
                PLUGIN.getAbstractDatabase().updateSQL(sql);
                callback.call(expiredItem);
            } catch (Exception ex) {
                ex.printStackTrace();
                callback.call(null);
            }
        });
    }

    @Override
    public ListedItem getItem(UUID key, Callback<ListedItem> callback) {
        String sql = "SELECT * FROM items WHERE uuid='" + key + "' LIMIT 1";
        ListedItem listedItem = null;

        try {
            ResultSet resultSet = PLUGIN.getAbstractDatabase().querySQL(sql);
            resultSet.next();

            OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(UUID.fromString(resultSet.getString("merchant")));
            ItemStack itemStack = ItemStackReflection.deserializeItemStack(resultSet.getString("item"));
            int price = resultSet.getInt("price");
            long expireTime = resultSet.getLong("expired_time");

            listedItem = new ListedItem(expireTime, itemStack, key, offlinePlayer, price);

            callback.call(listedItem);
        } catch (Exception ex) {
            ex.printStackTrace();
            callback.call(null);
        }

        return listedItem;
    }

    @Override
    public ExpiredItem getExpiredItem(UUID key, Callback<ExpiredItem> callback) {
        String sql = "SELECT * FROM expired_items WHERE uuid='" + key + "' LIMIT 1";
        ExpiredItem expiredItem = null;

        try {
            ResultSet resultSet = PLUGIN.getAbstractDatabase().querySQL(sql);
            resultSet.next();

            OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(UUID.fromString(resultSet.getString("merchant")));
            ItemStack itemStack = ItemStackReflection.deserializeItemStack(resultSet.getString("item"));
            long expireTime = resultSet.getLong("expired_time");
            long witherOutTime = resultSet.getLong("wither_out_time");

            expiredItem = new ExpiredItem(key, itemStack, expireTime, witherOutTime, offlinePlayer);
            callback.call(expiredItem);
        } catch (Exception ex) {
            ex.printStackTrace();
            callback.call(null);
        }

        return expiredItem;
    }

    @Override
    public void deleteItem(UUID key, Callback<Boolean> callback) {
        String sql = "DELETE FROM items WHERE uuid='" + key.toString() + "'";

        try {
            PLUGIN.getAbstractDatabase().executeSQL(sql);
            callback.call(true);
        } catch (Exception ex) {
            ex.printStackTrace();
            callback.call(false);
        }
    }

    @Override
    public void deleteExpiredItem(UUID key, Callback<Boolean> callback) {
        String sql = "DELETE FROM expired_items WHERE uuid='" + key.toString() + "'";

        try {
            PLUGIN.getAbstractDatabase().executeSQL(sql);
            callback.call(true);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            callback.call(false);
        }
    }

    @Override
    public void loadItemsFromStorage(Map<Long, ListedItem> mapToLoad, Callback<Map<Long, ListedItem>> callback) {
        String sql = "SELECT * FROM items";

        try {
            ResultSet resultSet = PLUGIN.getAbstractDatabase().querySQL(sql);

            while(resultSet.next()) {
                UUID itemUUID = UUID.fromString(resultSet.getString("uuid"));
                UUID merchantUUID = UUID.fromString(resultSet.getString("merchant"));
                ItemStack itemStack = ItemStackReflection.deserializeItemStack(resultSet.getString("item"));
                int price = resultSet.getInt("price");
                long expiredTime = resultSet.getLong("expired_time");
                ListedItem listedItem = new ListedItem(expiredTime, itemStack, itemUUID, Bukkit.getOfflinePlayer(merchantUUID), price);

                mapToLoad.put(expiredTime, listedItem);
            }

            callback.call(mapToLoad);
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
            callback.call(null);
        }
    }

    @Override
    public void loadExpiredItemsFromStorage(Map<Long, ExpiredItem> mapToLoad, Callback<Map<Long, ExpiredItem>> callback) {
        String sql = "SELECT * FROM expired_items";

        try {
            ResultSet resultSet = PLUGIN.getAbstractDatabase().querySQL(sql);

            while(resultSet.next()) {
                UUID itemUUID = UUID.fromString(resultSet.getString("uuid"));
                UUID merchant = UUID.fromString(resultSet.getString("merchant"));
                ItemStack itemStack = ItemStackReflection.deserializeItemStack(resultSet.getString("item"));
                long expiredTime = resultSet.getLong("expired_time");
                long witherOutTime = resultSet.getLong("wither_out_time");

                if(System.currentTimeMillis() > witherOutTime) {
                    PLUGIN.getLogger().log(Level.INFO, "Deleting expiredItem due to withering out...");
                    deleteExpiredItem(itemUUID, object -> {
                        if(object) PLUGIN.getLogger().log(Level.INFO, "> Successfully deleted.");
                        else PLUGIN.getLogger().log(Level.WARNING, "> An error occurred while deleting!");
                    });
                    continue;
                }

                ExpiredItem expiredItem = new ExpiredItem(itemUUID, itemStack, expiredTime, witherOutTime, Bukkit.getOfflinePlayer(merchant));

                mapToLoad.put(witherOutTime, expiredItem);
            }

            callback.call(mapToLoad);
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
            callback.call(null);
        }
    }



}
