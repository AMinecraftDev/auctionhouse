package net.aminecraftdev.auctionhouse.handlers.gui;

import net.aminecraftdev.auctionhouse.handlers.AuctionPlayer;
import net.aminecraftdev.auctionhouse.handlers.ExpiredItem;
import net.aminecraftdev.auctionhouse.handlers.GUIHandler;
import net.aminecraftdev.auctionhouse.managers.AuctionDataManager;
import net.aminecraftdev.auctionhouse.utils.core.Message;
import net.aminecraftdev.auctionhouse.utils.inventory.InventoryBuilder;
import net.aminecraftdev.auctionhouse.utils.inventory.Panel;
import net.aminecraftdev.auctionhouse.utils.itemstack.ItemStackUtils;
import net.aminecraftdev.auctionhouse.utils.message.MessageUtils;
import net.aminecraftdev.auctionhouse.utils.time.TimeUnit;
import net.aminecraftdev.auctionhouse.utils.time.TimeUtil;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 21-Sep-17
 */
public class ExpiredItemsHandler extends GUIHandler {

    private static Map<Integer, ConfigurationSection> updateSlots = new HashMap<>();
    private static Map<AuctionPlayer, Integer> currentPages = new HashMap<>();
    private static List<Integer> backSlots = new ArrayList<>();
    private static ConfigurationSection inventorySection;
    private static List<String> expiredLore;
    private static int itemsPerPage;

    private AuctionPlayer auctionPlayer;
    private Player player;
    private Panel panel;

    public ExpiredItemsHandler(Player player) {
        this.player = player;
        this.auctionPlayer = AuctionPlayer.getAuctionPlayer(player.getUniqueId());
    }

    /**
     * Opens the expiredItems menu
     * for the specified player.
     *
     */
    public void open() {
        Map<Long, ExpiredItem> expiredItemMap = auctionPlayer.getExpiredItems();
        Map<String, String> replaceMap = new HashMap<>();
        int maxPage = (int) Math.ceil((double) expiredItemMap.size()/(double) itemsPerPage) - 1;

        replaceMap.put("{page}", "1");
        replaceMap.put("{maxPage}", ((maxPage+1)==0? 1 : maxPage+1)+"");

        panel = new InventoryBuilder(inventorySection, replaceMap).getInventory()
                .setDestroyWhenDone(true)
                .setCancelClick(true);

        currentPages.put(auctionPlayer, 1);

        panel.setOnPageChange((p, currentPage, requestedPage) -> {
            if(requestedPage < 0 || requestedPage > maxPage) {
                return false;
            }

            currentPages.put(auctionPlayer, (requestedPage+1));
            loadPage(requestedPage, expiredItemMap);

            for(Map.Entry<Integer, ConfigurationSection> entry : updateSlots.entrySet()) {
                int slot = entry.getKey();

                panel.setItem(slot, getUpdatedItemStack(entry.getValue(), maxPage));
            }

            return true;
        });

        for(int i : backSlots) {
            panel.setOnClick(i, event -> new ListHandler(player).open());
        }

        loadPage(0, expiredItemMap);
        panel.openFor(player);
    }

    /**
     * Load the specified page for the specified panel
     * with the map of items.
     *
     * @param page - the page number to load (0 is always the bottom)
     * @param expiredItemMap - the map which is used to add data with.
     */
    private void loadPage(int page, Map<Long, ExpiredItem> expiredItemMap) {
        int startIndex = page * itemsPerPage;
        List<Long> keys = new ArrayList<>(expiredItemMap.keySet());

        for(int i = startIndex; i < startIndex + itemsPerPage; i++) {
            if(i >= keys.size()) {
                panel.setItem(i-startIndex, new ItemStack(Material.AIR), e -> {});
            } else {
                long witherTime = keys.get(i);
                ExpiredItem expiredItem = expiredItemMap.get(witherTime);

                if(System.currentTimeMillis() > witherTime) {
                    expiredItem.remove();
                    loadPage(page, expiredItemMap);
                    return;
                }

                panel.setItem(i-startIndex, getExpiredItemStack(expiredItem), event -> {
                    Player player = (Player) event.getWhoClicked();

                    if(player.getInventory().firstEmpty() == -1) {
                        Message.INVENTORY_SPACE.msg(player);
                        return;
                    }

                    ItemStack itemStack = expiredItem.getItemStack();

                    player.getInventory().addItem(itemStack);
                    auctionDataManager.deleteExpiredItem(expiredItem);
                    loadPage(page, auctionPlayer.getExpiredItems());
                });
            }
        }
    }

    /**
     * Get an expired itemstack
     * which is used in the expired item
     * list to show information about the
     * expiredItem.
     *
     * @param expiredItem - The expiredItem which is being checked for times.
     * @return the new ItemStack.
     */
    private ItemStack getExpiredItemStack(ExpiredItem expiredItem) {
        ItemStack itemStack = expiredItem.getItemStack().clone();
        ItemMeta itemMeta = itemStack.getItemMeta();

        long saleEndedAgo = System.currentTimeMillis() - expiredItem.getExpiryTime();
        long witherOutTime = expiredItem.getWitherOutTime() - System.currentTimeMillis();
        String formattedSaleEnded = TimeUtil.getFormattedTime(TimeUnit.MILLISECONDS, (int) saleEndedAgo);
        String formattedWitherOut = TimeUtil.getFormattedTime(TimeUnit.MILLISECONDS, (int) witherOutTime);
        List<String> lore = itemMeta.hasLore()? itemMeta.getLore() : new ArrayList<>();

        expiredLore.forEach(s -> lore.add(s
                .replace("{saleEnded}", formattedSaleEnded)
                .replace("{witherOutTime}", formattedWitherOut)
        ));

        itemMeta.setLore(lore);
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    /**
     * Get an updated ExpiredItem itemstack
     * which replaces the placeholders
     * with new data.
     *
     * @param configurationSection - the configurationSection for the item.
     * @param maxPage - The maximum page number.
     * @return the new ItemStack.
     */
    private ItemStack getUpdatedItemStack(ConfigurationSection configurationSection, int maxPage) {
        Map<String, String> replaceMap = new HashMap<>();

        replaceMap.put("{page}", currentPages.get(auctionPlayer)+"");
        replaceMap.put("{maxPage}", ((maxPage+1)==0? 1 : maxPage+1)+"");

        return ItemStackUtils.createItemStack(configurationSection, 1, replaceMap);
    }

    /**
     * Load all fields that are required
     * for this class.
     */
    public static void load() {
        expiredLore = new ArrayList<>();

        inventorySection = PLUGIN.getInventories().getConfigurationSection("ExpiredItemsGUI");
        itemsPerPage = PLUGIN.getConfig().getInt("Settings.expiredItemsPerPage");

        PLUGIN.getConfig().getStringList("LoreAddons.ExpiredItem").forEach(s -> expiredLore.add(MessageUtils.translateString(s)));

        ConfigurationSection items = inventorySection.getConfigurationSection("Items");

        items.getKeys(false).forEach(string -> {
            ConfigurationSection innerSection = items.getConfigurationSection(string);
            int slot = Integer.valueOf(string) - 1;

            if(innerSection.contains("Back") && innerSection.getBoolean("Back")) {
                backSlots.add(slot);
            }

            if(innerSection.contains("UpdateOnPageChange") && innerSection.getBoolean("UpdateOnPageChange")) {
                updateSlots.put(slot, innerSection);
            }
        });
    }
}
