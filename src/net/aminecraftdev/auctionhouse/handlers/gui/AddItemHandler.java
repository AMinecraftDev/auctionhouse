package net.aminecraftdev.auctionhouse.handlers.gui;

import net.aminecraftdev.auctionhouse.handlers.AuctionPlayer;
import net.aminecraftdev.auctionhouse.handlers.GUIHandler;
import net.aminecraftdev.auctionhouse.handlers.ItemListing;
import net.aminecraftdev.auctionhouse.handlers.ListedItem;
import net.aminecraftdev.auctionhouse.managers.AuctionDataManager;
import net.aminecraftdev.auctionhouse.utils.NumberUtils;
import net.aminecraftdev.auctionhouse.utils.core.Message;
import net.aminecraftdev.auctionhouse.utils.dependencies.VaultHelper;
import net.aminecraftdev.auctionhouse.utils.inventory.InventoryBuilder;
import net.aminecraftdev.auctionhouse.utils.inventory.Panel;
import net.aminecraftdev.auctionhouse.utils.itemstack.ItemStackUtils;
import net.aminecraftdev.auctionhouse.utils.time.TimeUnit;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 21-Sep-17
 */
public class AddItemHandler extends GUIHandler {

    private static List<Integer> listingHoursSlots, listingItemSlots, cancelListingSlots, confirmListingSlots, changePriceSlots;
    private static Map<Integer, ConfigurationSection> updateSlots = new HashMap<>();
    private static ConfigurationSection inventorySection;
    private static double hourlyPrice, listingFee;

    private AuctionPlayer auctionPlayer;
    private ItemListing itemListing;
    private Player player;
    private Panel panel;

    public AddItemHandler(Player player, ItemListing itemListing) {
        this.auctionPlayer = AuctionPlayer.getAuctionPlayer(player.getUniqueId());
        this.itemListing = itemListing;
        this.player = player;
    }

    /**
     * Open a pre-defined itemMenu with the
     * specified itemListing.
     */
    public void open() {
        Map<String, String> replaceMap = new HashMap<>();

        replaceMap.put("{playerName}", player.getName());
        replaceMap.put("{playerBalance}", NumberUtils.formatDouble(VaultHelper.getEconomy().getBalance(player)));
        replaceMap.put("{currentListingsAmount}", auctionPlayer.getCurrentListings()+"");
        replaceMap.put("{maxListingsAmount}", auctionPlayer.getMaxListings()+"");
        replaceMap.put("{listingHours}", itemListing.getListingHours()+"");
        replaceMap.put("{hourlyPrice}", NumberUtils.formatDouble(hourlyPrice));
        replaceMap.put("{listedPrice}", NumberUtils.formatDouble(itemListing.getPrice()));
        replaceMap.put("{listingHoursFee}", NumberUtils.formatDouble(hourlyPrice * itemListing.getListingHours()));
        replaceMap.put("{listingFee}", NumberUtils.formatDouble(listingFee));
        replaceMap.put("{totalPrice}", NumberUtils.formatDouble(getTotalFees()));

        panel = new InventoryBuilder(inventorySection, replaceMap).getInventory()
                .setDestroyWhenDone(true)
                .setCancelClick(true);

        for(int i : listingHoursSlots) {
            panel.setOnClick(i, event -> {
                ClickType clickType = event.getClick();

                if(clickType.toString().contains("LEFT")) {
                    if(!itemListing.decreaseListingHours()) {
                        Message.AddItem_BelowMinListingHours.msg(player);
                    }
                } else if(clickType.toString().contains("RIGHT")) {
                    if(!itemListing.increaseListingHours()) {
                        Message.AddItem_ExceedsMaxListingHours.msg(player);
                    }
                }

                for(Map.Entry<Integer, ConfigurationSection> entry : updateSlots.entrySet()) {
                    int slot = entry.getKey();
                    ConfigurationSection configurationSection = entry.getValue();

                    panel.setItem(slot, getUpdatedItemStack(configurationSection));
                }
            });
        }

        for(int i : listingItemSlots) {
            if(itemListing.getItemStack() != null) {
                panel.setItem(i, itemListing.getItemStack());
            }

            panel.setOnClick(i, event -> {
                if(event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR) return;
                if(itemListing.getItemStack() == null) return;

                player.getInventory().addItem(itemListing.getItemStack());
                itemListing.setItemStack(null);
                panel.setItem(i, null);

                for(Map.Entry<Integer, ConfigurationSection> entry : updateSlots.entrySet()) {
                    int slot = entry.getKey();
                    ConfigurationSection configurationSection = entry.getValue();

                    panel.setItem(slot, getUpdatedItemStack(configurationSection));
                }
            });
        }

        for(int i : changePriceSlots) {
            panel.setOnClick(i, event -> new ChangePriceHandler(player, itemListing).open());
        }

        for(int i : cancelListingSlots) {
            panel.setOnClick(i, event -> {
                if(itemListing.getItemStack() != null) {
                    player.getInventory().addItem(itemListing.getItemStack());
                }

                new ListHandler(player).open();
            });
        }

        for(int i : confirmListingSlots) {
            panel.setOnClick(i, event -> {
                Player p = (Player) event.getWhoClicked();
                double balance = VaultHelper.getEconomy().getBalance(p);
                double listingFees = listingFee + (hourlyPrice * itemListing.getListingHours());
                int currentListed = auctionPlayer.getCurrentListings();
                int maxListing = auctionPlayer.getMaxListings();

                if(currentListed+1 > maxListing) {
                    Message.AddItem_TooManyListed.msg(p);
                    new ListHandler(player).open();
                    return;
                }

                if(balance < listingFees) {
                    Message.AddItem_NotEnoughMoney.msg(p);
                    new ListHandler(player).open();
                    return;
                }

                if(itemListing.getItemStack() == null || itemListing.getItemStack().getType() == Material.AIR) {
                    Message.AddItem_CannotListNull.msg(p);
                    return;
                }

                VaultHelper.getEconomy().withdrawPlayer(p, listingFees);

                long listingHours = (long) TimeUnit.HOURS.to(TimeUnit.MILLISECONDS, itemListing.getListingHours());
                long expireInMillis = System.currentTimeMillis() + listingHours;

                ListedItem listedItem = new ListedItem(expireInMillis, itemListing.getItemStack(), itemListing.getPlayer(), itemListing.getPrice());

                auctionDataManager.saveItem(listedItem);
                auctionPlayer.increaseCurrentListings();

                Message.AddItem_Listed.msg(p, NumberUtils.formatDouble(itemListing.getPrice()), itemListing.getListingHours());
                Message.MONEY_TAKEN.msg(p, NumberUtils.formatDouble(listingFees));

                new ListHandler(player).open();
            });
        }

        panel.setOnClick(event -> {
            Player p = (Player) event.getWhoClicked();
            int rawSlot = event.getRawSlot();
            int inventorySize = p.getOpenInventory().getTopInventory().getSize();

            if(rawSlot < inventorySize) return;

            ItemStack itemStack = event.getCurrentItem();

            if(itemStack == null || itemStack.getType() == Material.AIR) return;

            for(int i : listingItemSlots) {
                panel.setItem(i, itemStack.clone());
            }

            if(itemListing.getItemStack() != null) {
                ItemStack currentListedItemStack = itemListing.getItemStack().clone();
                p.getInventory().addItem(currentListedItemStack);
            }

            event.setCurrentItem(new ItemStack(Material.AIR));
            itemListing.setItemStack(itemStack);
        });

        panel.openFor(player);
    }

    /**
     * Get the total fees amount for an
     * itemListing.
     *
     * @return a double amount which is the total fees.
     */
    private double getTotalFees() {
        return listingFee + (hourlyPrice * itemListing.getListingHours());
    }

    /**
     * Get an updated AddItem itemstack
     * which replaces the placeholders
     * with new data.
     *
     * @param configurationSection - the configurationSection for the item.
     * @return the new ItemStack.
     */
    private ItemStack getUpdatedItemStack(ConfigurationSection configurationSection) {
        Map<String, String> replaceMap = new HashMap<>();

        replaceMap.put("{playerName}", itemListing.getPlayer().getName());
        replaceMap.put("{playerBalance}", NumberUtils.formatDouble(VaultHelper.getEconomy().getBalance(itemListing.getPlayer())));
        replaceMap.put("{currentListingsAmount}", auctionPlayer.getCurrentListings()+"");
        replaceMap.put("{maxListingsAmount}", auctionPlayer.getMaxListings()+"");
        replaceMap.put("{listingHours}", itemListing.getListingHours()+"");
        replaceMap.put("{hourlyPrice}", NumberUtils.formatDouble(hourlyPrice));
        replaceMap.put("{listedPrice}", NumberUtils.formatDouble(itemListing.getPrice()));
        replaceMap.put("{listingHoursFee}", NumberUtils.formatDouble(hourlyPrice * itemListing.getListingHours()));
        replaceMap.put("{listingFee}", NumberUtils.formatDouble(listingFee));
        replaceMap.put("{totalPrice}", NumberUtils.formatDouble(listingFee + (hourlyPrice * itemListing.getListingHours())));

        return ItemStackUtils.createItemStack(configurationSection, 1, replaceMap);
    }

    /**
     * Load all fields that are required
     * for this class.
     */
    public static void load() {
        listingHoursSlots = new ArrayList<>();
        listingItemSlots = new ArrayList<>();
        cancelListingSlots = new ArrayList<>();
        confirmListingSlots = new ArrayList<>();
        changePriceSlots = new ArrayList<>();

        inventorySection = PLUGIN.getInventories().getConfigurationSection("AddItemGUI");

        hourlyPrice = PLUGIN.getConfig().getDouble("Settings.ListHours.feePerHour");
        listingFee = PLUGIN.getConfig().getDouble("Settings.ListHours.listingFee");

        ConfigurationSection items = inventorySection.getConfigurationSection("Items");

        items.getKeys(false).forEach(string -> {
            ConfigurationSection innerSection = items.getConfigurationSection(string);
            int slot = Integer.valueOf(string) - 1;

            if(innerSection.contains("ListingHours") && innerSection.getBoolean("ListingHours")) {
                listingHoursSlots.add(slot);
            }

            if(innerSection.contains("ListingItem") && innerSection.getBoolean("ListingItem")) {
                listingItemSlots.add(slot);
            }

            if(innerSection.contains("ChangePrice") && innerSection.getBoolean("ChangePrice")) {
                changePriceSlots.add(slot);
            }

            if(innerSection.contains("Cancel") && innerSection.getBoolean("Cancel")) {
                cancelListingSlots.add(slot);
            }

            if(innerSection.contains("Confirm") && innerSection.getBoolean("Confirm")) {
                confirmListingSlots.add(slot);
            }

            if(innerSection.contains("UpdateOnClick") && innerSection.getBoolean("UpdateOnClick")) {
                updateSlots.put(slot, innerSection);
            }
        });
    }
}
