package net.aminecraftdev.auctionhouse.handlers.gui;

import net.aminecraftdev.auctionhouse.handlers.AuctionPlayer;
import net.aminecraftdev.auctionhouse.handlers.GUIHandler;
import net.aminecraftdev.auctionhouse.handlers.ItemListing;
import net.aminecraftdev.auctionhouse.utils.NumberUtils;
import net.aminecraftdev.auctionhouse.utils.dependencies.VaultHelper;
import net.aminecraftdev.auctionhouse.utils.inventory.InventoryBuilder;
import net.aminecraftdev.auctionhouse.utils.inventory.Panel;
import net.aminecraftdev.auctionhouse.utils.itemstack.ItemStackUtils;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 21-Sep-17
 */
public class ChangePriceHandler extends GUIHandler {

    private static Map<Integer, ConfigurationSection> updateSlots = new HashMap<>();
    private static List<Integer> confirmChangeSlots, cancelChangeSlots;
    private static Map<Integer, Double> changeMap = new HashMap<>();
    private static ConfigurationSection inventorySection;
    private static double minPrice, maxPrice;

    private ItemListing itemListing;
    private Player player;
    private Panel panel;

    public ChangePriceHandler(Player player, ItemListing itemListing) {
        this.itemListing = itemListing;
        this.player = player;
    }

    /**
     * Opens a menu which allows you to change
     * the listing price of the itemListing.
     */
    public void open() {
        Map<String, String> replaceMap = new HashMap<>();
        double beforeChangePrice = itemListing.getPrice();

        replaceMap.put("{balance}", NumberUtils.formatDouble(VaultHelper.getEconomy().getBalance(player)));
        replaceMap.put("{totalPrice}", NumberUtils.formatDouble(itemListing.getPrice()));

        panel = new InventoryBuilder(inventorySection, replaceMap).getInventory()
                .setCancelClick(true)
                .setDestroyWhenDone(true);

        panel.setOnClose(p -> Bukkit.getScheduler().runTaskLater(PLUGIN, () -> new AddItemHandler(player, itemListing).open(), 1L));

        for(int i : cancelChangeSlots) {
            panel.setOnClick(i, event -> {
                itemListing.setPrice(beforeChangePrice);
                player.closeInventory();
            });
        }

        for(int i : confirmChangeSlots) {
            panel.setOnClick(i, event -> player.closeInventory());
        }

        for(Map.Entry<Integer, Double> entry : changeMap.entrySet()) {
            int slot = entry.getKey();
            double changeValue = entry.getValue();

            panel.setOnClick(slot, event -> {
                ClickType clickType = event.getClick();
                double current = itemListing.getPrice();

                if(clickType.toString().contains("LEFT")) {
                    if((current - changeValue) < minPrice) return;

                    itemListing.setPrice(current - changeValue);
                }

                if(clickType.toString().contains("RIGHT")) {
                    if((current + changeValue) > maxPrice) return;

                    itemListing.setPrice(current + changeValue);
                }

                for(Map.Entry<Integer, ConfigurationSection> entry1 : updateSlots.entrySet()) {
                    panel.setItem(entry1.getKey(), getUpdatedItemStack(entry1.getValue()));
                }
            });
        }

        panel.openFor(player);
    }

    /**
     * Get an updated AddItem itemstack
     * which replaces the placeholders
     * with new data.
     *
     * @param configurationSection - the configurationSection for the item.
     * @return the new ItemStack.
     */
    private ItemStack getUpdatedItemStack(ConfigurationSection configurationSection) {
        Map<String, String> replaceMap = new HashMap<>();

        replaceMap.put("{balance}", NumberUtils.formatDouble(VaultHelper.getEconomy().getBalance(itemListing.getPlayer())));
        replaceMap.put("{totalPrice}", NumberUtils.formatDouble(itemListing.getPrice()));

        return ItemStackUtils.createItemStack(configurationSection, 1, replaceMap);
    }

    /**
     * Load all fields that are required
     * for this class.
     */
    public static void load() {
        confirmChangeSlots = new ArrayList<>();
        cancelChangeSlots = new ArrayList<>();

        inventorySection = PLUGIN.getInventories().getConfigurationSection("ChangePriceGUI");
        minPrice = PLUGIN.getConfig().getDouble("Settings.Limits.minSellPrice");
        maxPrice = PLUGIN.getConfig().getDouble("Settings.Limits.maxSellPrice");

        ConfigurationSection items = inventorySection.getConfigurationSection("Items");

        items.getKeys(false).forEach(string -> {
            ConfigurationSection innerSection = items.getConfigurationSection(string);
            int slot = Integer.valueOf(string) - 1;

            if(innerSection.contains("Change")) {
                changeMap.put(slot, innerSection.getDouble("Change"));
            }

            if(innerSection.contains("Cancel") && innerSection.getBoolean("Cancel")) {
                cancelChangeSlots.add(slot);
            }

            if(innerSection.contains("Confirm") && innerSection.getBoolean("Confirm")) {
                confirmChangeSlots.add(slot);
            }

            if(innerSection.contains("UpdateOnClick") && innerSection.getBoolean("UpdateOnClick")) {
                updateSlots.put(slot, innerSection);
            }
        });
    }

}
