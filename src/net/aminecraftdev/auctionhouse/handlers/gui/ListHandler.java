package net.aminecraftdev.auctionhouse.handlers.gui;

import net.aminecraftdev.auctionhouse.handlers.*;
import net.aminecraftdev.auctionhouse.managers.AuctionDataManager;
import net.aminecraftdev.auctionhouse.utils.NumberUtils;
import net.aminecraftdev.auctionhouse.utils.core.Message;
import net.aminecraftdev.auctionhouse.utils.inventory.InventoryBuilder;
import net.aminecraftdev.auctionhouse.utils.inventory.Panel;
import net.aminecraftdev.auctionhouse.utils.itemstack.ItemStackUtils;
import net.aminecraftdev.auctionhouse.utils.message.MessageUtils;
import net.aminecraftdev.auctionhouse.utils.time.TimeUnit;
import net.aminecraftdev.auctionhouse.utils.time.TimeUtil;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 21-Sep-17
 */
public class ListHandler extends GUIHandler {

    private static Map<Integer, ConfigurationSection> updateSlots = new HashMap<>();
    private static Map<AuctionPlayer, Integer> currentPages = new HashMap<>();
    private static List<String> adminLore, purchaseLore, ownItemLore;
    private static ConfigurationSection inventorySection;
    private static List<Integer> addItem, expired;
    private static int itemsPerPage;

    private AuctionPlayer auctionPlayer;
    private Player player;
    private Panel panel;

    /**
     * Manages the list for the specified player
     *
     * @param player - the player to work with.
     */
    public ListHandler(Player player) {
        this.player = player;
        this.auctionPlayer = AuctionPlayer.getAuctionPlayer(this.player.getUniqueId());
    }

    /**
     * Opens the main auction page
     * for the specified player.
     *
     */
    @Override
    public void open() {
        Map<Long, ListedItem> listedItemMap = new TreeMap<>(auctionDataManager.getItemListingMap());
        int maxPage = (int) Math.ceil((double) listedItemMap.size()/(double) itemsPerPage) - 1;
        Map<String, String> replaceMap = new HashMap<>();

        replaceMap.put("{page}", "1");
        replaceMap.put("{maxPage}", ((maxPage+1)==0? 1 : maxPage+1)+"");

        this.panel = new InventoryBuilder(inventorySection, replaceMap).getInventory()
                .setCancelClick(true)
                .setDestroyWhenDone(true);

        currentPages.put(auctionPlayer, 1);

        this.panel.setOnPageChange((p, currentPage, requestedPage) -> {
            if(requestedPage < 0 || requestedPage > maxPage) {
                return false;
            }

            currentPages.put(auctionPlayer, (requestedPage+1));
            loadPage(requestedPage, listedItemMap);

            for(Map.Entry<Integer, ConfigurationSection> entry : updateSlots.entrySet()) {
                int slot = entry.getKey();

                this.panel.setItem(slot, getUpdatedListItemStack(entry.getValue(), maxPage));
            }

            return true;
        });

        for(int i : expired) {
            this.panel.setOnClick(i, event -> new ExpiredItemsHandler(player).open());
        }

        for(int i : addItem) {
            this.panel.setOnClick(i, event -> new AddItemHandler(player, new ItemListing(player)).open());
        }

        loadPage(0, listedItemMap);
        this.panel.openFor(player);
    }

    /**
     * loads the page of data in with the
     * specified entries.
     *
     * @param page - the page number to load.
     * @param listedItemMap - the map of data to get from.
     */
    private void loadPage(int page, Map<Long, ListedItem> listedItemMap) {
        int startIndex = page * itemsPerPage;
        List<Long> keys = new ArrayList<>(listedItemMap.keySet());

        for(int i = startIndex; i < startIndex + itemsPerPage; i++) {
            if(i >= keys.size()) {
                this.panel.setItem(i-startIndex, new ItemStack(Material.AIR), event -> {});
            } else {
                long expireTime = keys.get(i);
                ListedItem listedItem = listedItemMap.get(expireTime);

                this.panel.setItem(i-startIndex, getListItemStack(listedItem), event -> {
                    Player player = (Player) event.getWhoClicked();
                    ClickType clickType = event.getClick();

                    if(!auctionDataManager.getItemListingMap().containsKey(expireTime)) {
                        Message.NoLongerForSale.msg(player);
                        return;
                    }

                    boolean isMerchant = listedItem.getMerchant().getUniqueId().equals(auctionPlayer.getOfflinePlayer().getUniqueId());
                    boolean isAdmin = auctionPlayer.getOfflinePlayer().getPlayer().hasPermission("ah.admin");

                    if(isAdmin && clickType == ClickType.SHIFT_RIGHT) {
                        listedItem.adminCancel(player);
                        return;
                    }

                    if(isMerchant) {
                        listedItem.cancel(player);
                        return;
                    }

                    listedItem.purchase(player);
                });
            }
        }
    }

    /**
     * Returns a modified itemstack of a list
     * entry which is used for display.
     *
     * @param listedItem - the listedItem to check for
     * @return the new ItemStack
     */
    private ItemStack getListItemStack(ListedItem listedItem) {
        ItemStack itemStack = listedItem.getItemStack().clone();
        ItemMeta itemMeta = itemStack.getItemMeta();
        boolean isMerchant = listedItem.getMerchant().getUniqueId().equals(auctionPlayer.getOfflinePlayer().getUniqueId());
        boolean isAdmin = auctionPlayer.getOfflinePlayer().getPlayer().hasPermission("ah.admin");
        long timeLeft = listedItem.getExpiryTime() - System.currentTimeMillis();
        String formattedTimeLeft = TimeUtil.getFormattedTime(TimeUnit.MILLISECONDS, (int) timeLeft);
        List<String> lore = itemMeta.hasLore()? itemMeta.getLore() : new ArrayList<>();

        if(isMerchant) {
            ownItemLore.forEach(s -> lore.add(s
                    .replace("{merchantName}", listedItem.getMerchant().getName())
                    .replace("{price}", NumberUtils.formatDouble(listedItem.getPrice()))
                    .replace("{timeLeft}", formattedTimeLeft)
            ));
        } else {
            purchaseLore.forEach(s -> lore.add(s
                    .replace("{merchantName}", listedItem.getMerchant().getName())
                    .replace("{price}", NumberUtils.formatDouble(listedItem.getPrice()))
                    .replace("{timeLeft}", formattedTimeLeft)
            ));
        }

        if(isAdmin) {
            adminLore.forEach(s -> lore.add(MessageUtils.translateString(s)));
        }

        itemMeta.setLore(lore);
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    /**
     * Returns an updated list itemstack
     * with new placeholder values.
     *
     * @param configurationSection - the configuration section the item is used to create.
     * @param maxPage - the maximum page number.
     * @return the updated ItemStack.
     */
    private ItemStack getUpdatedListItemStack(ConfigurationSection configurationSection, int maxPage) {
        Map<String, String> replaceMap = new HashMap<>();

        replaceMap.put("{page}", currentPages.get(auctionPlayer)+"");
        replaceMap.put("{maxPage}", ((maxPage+1)==0? 1 : maxPage+1)+"");

        return ItemStackUtils.createItemStack(configurationSection, 1, replaceMap);
    }

    /**
     * Load all fields that are required
     * for this class.
     */
    public static void load() {
        itemsPerPage = PLUGIN.getConfig().getInt("Settings.auctionItemsPerPage");
        inventorySection = PLUGIN.getInventories().getConfigurationSection("AuctionHouseGUI");

        ownItemLore = new ArrayList<>();
        adminLore = new ArrayList<>();
        purchaseLore = new ArrayList<>();
        addItem = new ArrayList<>();
        expired = new ArrayList<>();

        PLUGIN.getConfig().getStringList("LoreAddons.AdminLore").forEach(s -> adminLore.add(MessageUtils.translateString(s)));
        PLUGIN.getConfig().getStringList("LoreAddons.PurchaseLore").forEach(s -> purchaseLore.add(MessageUtils.translateString(s)));
        PLUGIN.getConfig().getStringList("LoreAddons.OwnItem").forEach(s -> ownItemLore.add(MessageUtils.translateString(s)));

        ConfigurationSection items = inventorySection.getConfigurationSection("Items");

        items.getKeys(false).forEach(string -> {
            ConfigurationSection innerSection = items.getConfigurationSection(string);
            int slot = Integer.valueOf(string) - 1;

            if(innerSection.contains("UpdateOnPageChange") && innerSection.getBoolean("UpdateOnPageChange")) {
                updateSlots.put(slot, innerSection);
            }

            if(innerSection.contains("AddItem") && innerSection.getBoolean("AddItem")) {
                addItem.add(slot);
            }

            if(innerSection.contains("Expired") && innerSection.getBoolean("Expired")) {
                expired.add(slot);
            }
        });
    }

}
