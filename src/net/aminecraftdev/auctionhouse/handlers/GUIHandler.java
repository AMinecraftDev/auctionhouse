package net.aminecraftdev.auctionhouse.handlers;

import net.aminecraftdev.auctionhouse.AuctionHouse;
import net.aminecraftdev.auctionhouse.managers.AuctionDataManager;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 21-Sep-17
 */
public abstract class GUIHandler {

    protected static AuctionHouse PLUGIN = AuctionHouse.getI();
    protected static AuctionDataManager auctionDataManager;

    public abstract void open();

    public static void load(AuctionDataManager aucDataManager) {
        auctionDataManager = aucDataManager;
    }

}
