package net.aminecraftdev.auctionhouse.handlers;

import net.aminecraftdev.auctionhouse.utils.Callback;
import org.bukkit.entity.Player;

import java.sql.SQLException;
import java.util.Map;
import java.util.UUID;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 20-Sep-17
 */
public interface DataManagement {

    /**
     * Used to create the databases if
     * they don't already exist.
     */
    void createTables();

    /**
     * Save a listedItem into the database,
     * this will override any previous row if
     * there is another row which has the same
     * primary key.
     *
     * @param listedItem - The item to save
     * @param callback - The callback to check to.
     */
    void saveItem(ListedItem listedItem, Callback<ListedItem> callback);

    /**
     * Save an expiredItem into the database,
     * this will override any previous row if
     * there is another row which has the same
     * primary key.
     *
     * @param expiredItem - The expiredItem being saved.
     * @param callback - The callback to check to.
     */
    void saveExpiredItem(ExpiredItem expiredItem, Callback<ExpiredItem> callback);

    /**
     * This will return a ListedItem object
     * of the item with they specified key.
     *
     * This will return null if there is no
     * row in the items table with the
     * specified key.
     *
     * @param key - the checked UUID.
     * @param callback - The callback to check to.
     * @return the ListedItem.
     */
    ListedItem getItem(UUID key, Callback<ListedItem> callback);

    /**
     * This will return an ExpiredItem object
     * of the item with they specified key.
     *
     * This will return null if there is no
     * row in the items table with the
     * specified key.
     *
     * @param key - the checked UUID.
     * @param callback - The callback to check to.
     * @return the ExpiredItem.
     */
    ExpiredItem getExpiredItem(UUID key, Callback<ExpiredItem> callback);

    /**
     * Delete the row in the item table
     * which has the specified key.
     *
     * @param key - The key that is checked.
     * @param callback - The callback to check to.
     */
    void deleteItem(UUID key, Callback<Boolean> callback);

    /**
     * Delete the row in the expired item
     * table which has the specified key.
     *
     * @param key - the key that is checked.
     * @param callback - The call back to check to.
     */
    void deleteExpiredItem(UUID key, Callback<Boolean> callback);

    /**
     * Get a map of all the items in the
     * database.
     *
     * @param mapToLoad - Map to load data in too.
     * @param callback - The callback to check to.
     */
    void loadItemsFromStorage(Map<Long, ListedItem> mapToLoad, Callback<Map<Long, ListedItem>> callback);

    /**
     * Get a map of all the expiredItems
     * which have the same uuid as the
     * checked player.
     *
     * @param mapToLoad - Map to load data in too.
     * @param callback - The callback to check to.
     */
    void loadExpiredItemsFromStorage(Map<Long, ExpiredItem> mapToLoad, Callback<Map<Long, ExpiredItem>> callback);

}
