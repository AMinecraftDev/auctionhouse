package net.aminecraftdev.auctionhouse.handlers;

import net.aminecraftdev.auctionhouse.managers.AuctionDataManager;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 20-Sep-17
 */
public class AuctionPlayer {

    private static final List<AuctionPlayer> auctionPlayers = new ArrayList<>();
    private static AuctionDataManager dataManager;

    private OfflinePlayer offlinePlayer;
    private int currentListings, maxListings;

    private AuctionPlayer(OfflinePlayer offlinePlayer, int currentListings) {
        this.offlinePlayer = offlinePlayer;
        this.currentListings = currentListings;

        setMaxListings();

        auctionPlayers.add(this);
    }

    private void setMaxListings() {
        if(offlinePlayer.isOnline()) {
            Player player = offlinePlayer.getPlayer();

            if(player.hasPermission("auction.limit.*")) {
                this.maxListings = 1000;
            } else {
                int max = 0;

                for(int i = 0; i <= 1000; i++) {
                    if(player.hasPermission("auction.limit." + i)) {
                        if(i > max) max = i;
                    }
                }

                this.maxListings = max;
            }
        } else {
            this.maxListings = 0;
        }
    }

    /**
     * @return the expiredItems map for this auctionPlayer.
     */
    public Map<Long, ExpiredItem> getExpiredItems() {
        return dataManager.getExpiredItems(offlinePlayer);
    }

    /**
     * Used to increase the current amount
     * of listings. (When adding a new item)
     */
    public void increaseCurrentListings() {
        this.currentListings++;
    }

    /**
     * Get the current number of listings.
     *
     * @return an int amount of currentListings.
     */
    public int getCurrentListings() {
        return currentListings;
    }

    /**
     * Decrease the current amount of listings.
     * (Used when an item is removed or purchased
     * from the list).
     */
    public void decreaseCurrentListings() {
        this.currentListings--;
    }

    /**
     * @return get's the players maximum listings amount.
     */
    public int getMaxListings() {
        if(maxListings <= 0) {
            setMaxListings();
        }

        return maxListings;
    }

    /**
     * Get an offlinePlayer instance of
     * the merchant.
     *
     * @return the offlinePlayer instance.
     */
    public OfflinePlayer getOfflinePlayer() {
        return offlinePlayer;
    }

    /**
     * Used to get an AuctionPlayer instance,
     * if it doesn't exist it'll create one
     * otherwise it'll pull the AuctionPlayer
     * from the lists.
     *
     * @param uuid - the AuctionPlayer uuid.
     * @return the AuctionPlayer instance.
     */
    public static AuctionPlayer getAuctionPlayer(UUID uuid) {
        for(AuctionPlayer auctionPlayer : auctionPlayers) {
            if(auctionPlayer.getOfflinePlayer().getUniqueId().equals(uuid)) return auctionPlayer;
        }

        return new AuctionPlayer(Bukkit.getOfflinePlayer(uuid), dataManager.getCurrentListings(uuid));
    }

    public static void load(AuctionDataManager auctionDataManager) {
        dataManager = auctionDataManager;
    }

}
