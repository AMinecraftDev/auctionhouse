package net.aminecraftdev.auctionhouse.handlers;

import net.aminecraftdev.auctionhouse.AuctionHouse;
import net.aminecraftdev.auctionhouse.managers.AuctionDataManager;
import net.aminecraftdev.auctionhouse.utils.time.TimeUnit;
import org.bukkit.OfflinePlayer;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 20-Sep-17
 */
public class ExpiredItem {

    private static final AuctionHouse PLUGIN = AuctionHouse.getI();

    private static AuctionDataManager auctionDataManager;
    private static long defaultWitherTime;

    private OfflinePlayer merchant;
    private UUID uuid;
    private ItemStack itemStack;
    private long expiryTime, witherOutTime;

    public ExpiredItem(UUID uuid, ItemStack itemStack, long expiredTime, OfflinePlayer merchant) {
        this(uuid, itemStack, expiredTime, expiredTime + defaultWitherTime, merchant);
    }

    public ExpiredItem(UUID uuid, ItemStack itemStack, long expiredTime, long witherOutTime, OfflinePlayer merchant) {
        this.uuid = uuid;
        this.merchant = merchant;
        this.itemStack = itemStack;
        this.expiryTime = expiredTime;
        this.witherOutTime = witherOutTime;
    }

    public OfflinePlayer getMerchant() {
        return merchant;
    }

    public UUID getUuid() {
        return uuid;
    }

    public long getExpiryTime() {
        return expiryTime;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public long getWitherOutTime() {
        return witherOutTime;
    }

    public void remove() {
        auctionDataManager.deleteExpiredItem(this);
    }

    public static void load(AuctionDataManager aucDataManager) {
        auctionDataManager = aucDataManager;
        defaultWitherTime = (long) TimeUnit.HOURS.to(TimeUnit.MILLISECONDS, PLUGIN.getConfig().getInt("Settings.unclaimedHoursBeforeExpire", 240));
    }

}
