package net.aminecraftdev.auctionhouse.managers;

import net.aminecraftdev.auctionhouse.AuctionHouse;
import net.aminecraftdev.auctionhouse.handlers.DataManagement;
import net.aminecraftdev.auctionhouse.handlers.ExpiredItem;
import net.aminecraftdev.auctionhouse.handlers.ListedItem;
import net.aminecraftdev.auctionhouse.utils.Callback;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;
import java.util.logging.Level;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 20-Sep-17
 */
public class AuctionDataManager {

    private static final AuctionHouse PLUGIN = AuctionHouse.getI();

    private static Map<Long, ListedItem> itemListingMap = new TreeMap<>();
    private static Map<Long, ExpiredItem> expiredItemMap = new TreeMap<>();
    private static DataManagement dataManagement;

    public AuctionDataManager(DataManagement dManagement) {
        dataManagement = dManagement;

        PLUGIN.getLogger().log(Level.INFO, "Loading expired items to map from database...");
        dManagement.loadExpiredItemsFromStorage(expiredItemMap, object -> {
            if(object == null || object.isEmpty()) PLUGIN.getLogger().log(Level.WARNING, "> Error occurred while trying to load expiredItems.");
            else PLUGIN.getLogger().log(Level.INFO, "> Successfully filled map with data.");
        });
        PLUGIN.getLogger().log(Level.INFO, "Loading listed items to map from database...");
        dManagement.loadItemsFromStorage(itemListingMap, object -> {
            if(object == null || object.isEmpty()) PLUGIN.getLogger().log(Level.WARNING, "> Error occurred while trying to load listedItems.");
            else PLUGIN.getLogger().log(Level.INFO, "> Successfully filled map with data.");
        });

        expireListings();
    }

    /**
     * @return the map of all current listedItems.
     */
    public Map<Long, ListedItem> getItemListingMap() {
        return itemListingMap;
    }

    /**
     * Returns an int amount of how
     * many ItemListings the player
     * with the specified uuid.
     *
     * @param uuid - the uuid to check for.
     * @return the amount of current listings for the player.
     */
    public int getCurrentListings(UUID uuid) {
        Iterator<Map.Entry<Long, ListedItem>> iterator = new HashMap<>(itemListingMap).entrySet().iterator();
        int currentListings = 0;

        while(iterator.hasNext()) {
            Map.Entry<Long, ListedItem> entry = iterator.next();
            OfflinePlayer merchant = entry.getValue().getMerchant();

            if(uuid.equals(merchant.getUniqueId())) currentListings++;
        }

        return currentListings;
    }

    /**
     * @param player - the player to check for.
     * @return a map of the expiredItems for the specified player.
     */
    public Map<Long, ExpiredItem> getExpiredItems(OfflinePlayer player) {
        Map<Long, ExpiredItem> newMap = new TreeMap<>();

        expiredItemMap.forEach((key, value) -> {
            if(value.getMerchant().getUniqueId().equals(player.getUniqueId())) newMap.put(key, value);
        });

        return newMap;
    }

    /**
     * @param listedItem - the listed item to save.
     */
    public void saveItem(ListedItem listedItem) {
        PLUGIN.getLogger().log(Level.INFO, "Attempting to save ListedItem to the database...");
        dataManagement.saveItem(listedItem, object -> {
            if(object == null) PLUGIN.getLogger().log(Level.WARNING, "> Error, Something went wrong while saving!");
            else PLUGIN.getLogger().log(Level.INFO, "> Save successful.");
        });

        itemListingMap.put(listedItem.getExpiryTime(), listedItem);
    }

    /**
     *
     * @param expiredItem - The expired item to be saved.
     */
    public void saveExpiredItem(ExpiredItem expiredItem) {
        PLUGIN.getLogger().log(Level.INFO, "Attempting to save ExpiredItem to the database...");
        dataManagement.saveExpiredItem(expiredItem, object -> {
            if(object == null) PLUGIN.getLogger().log(Level.WARNING, "> Error, something went wrong while saving!");
            else PLUGIN.getLogger().log(Level.INFO, "> Save successful.");
        });
    }

    /**
     *
     * @param uuid - the uuid to get ListedItem with.
     * @return the listedItem object.
     */
    public ListedItem getListedItem(UUID uuid) {
        PLUGIN.getLogger().log(Level.INFO, "Attempting to get ListedItem with uuid, '" + uuid.toString() + "'...");

        for(Map.Entry<Long, ListedItem> entry : new TreeMap<>(itemListingMap).entrySet()) {
            if(entry.getValue().getUuid().equals(uuid)) {
                PLUGIN.getLogger().log(Level.INFO, "> Item successfully pulled from current listed items.");
                return entry.getValue();
            }
        }

        ListedItem listedItem = dataManagement.getItem(uuid, object -> {
            if(object == null) PLUGIN.getLogger().log(Level.WARNING, "> Error, something went wrong while creating the item!");
            else PLUGIN.getLogger().log(Level.INFO, "> Item created successfully!");
        });

        if(listedItem != null) {
            itemListingMap.put(listedItem.getExpiryTime(), listedItem);
        }

        return listedItem;
    }

    /**
     *
     * @param uuid - The uuid to get ExpiredItem with.
     * @return the expiredItem object.
     */
    public ExpiredItem getExpiredItem(UUID uuid) {
        PLUGIN.getLogger().log(Level.INFO, "Attempting to get ExpiredItem with uuid, '" + uuid.toString() + "'...");

        for(Map.Entry<Long, ExpiredItem> entry : new TreeMap<>(expiredItemMap).entrySet()) {
            if(entry.getValue().getUuid().equals(uuid)) {
                PLUGIN.getLogger().log(Level.INFO, "> Item successfully pulled from current expired items.");
                return entry.getValue();
            }
        }

        ExpiredItem expiredItem = dataManagement.getExpiredItem(uuid, object -> {
            if(object == null) PLUGIN.getLogger().log(Level.WARNING, "> Error, something went wrong while getting the expiredItem from the database!");
            else PLUGIN.getLogger().log(Level.INFO, "> Item successfully pulled from the database.");
        });

        if(expiredItem != null) {
            expiredItemMap.put(expiredItem.getWitherOutTime(), expiredItem);
        }

        return expiredItem;
    }

    /**
     *
     * @param listedItem - The ListedItem to delete.
     */
    public void deleteItem(ListedItem listedItem) {
        PLUGIN.getLogger().log(Level.INFO, "Attempting to delete item with uuid, '" + listedItem.getUuid().toString() + "'...");
        dataManagement.deleteItem(listedItem.getUuid(), object -> {
            if(object) PLUGIN.getLogger().log(Level.INFO, "> Item successfully deleted.");
            else PLUGIN.getLogger().log(Level.WARNING, "> Error, something went wrong while deleting item!");
        });

        itemListingMap.remove(listedItem.getExpiryTime());
    }

    /**
     *
     * @param expiredItem - The expiredItem to delete.
     */
    public void deleteExpiredItem(ExpiredItem expiredItem) {
        PLUGIN.getLogger().log(Level.INFO, "Attempting to delete expiredItem with uuid, '" + expiredItem.getUuid().toString() + "'...");
        dataManagement.deleteExpiredItem(expiredItem.getUuid(), object -> {
            if(object) PLUGIN.getLogger().log(Level.INFO, "> Item was successfully deleted.");
            else PLUGIN.getLogger().log(Level.WARNING, "> Error, something went wrong while deleting expired item!");
        });

        expiredItemMap.remove(expiredItem.getWitherOutTime());
    }

    /**
     * This will create a new ExpiredItem,
     * to replace the currently specified
     * ListedItem.
     *
     * @param listedItem - the ListedItem to get data from.
     */
    public void createExpiredItem(ListedItem listedItem) {
        ExpiredItem expiredItem = new ExpiredItem(listedItem.getUuid(), listedItem.getItemStack(), System.currentTimeMillis(), listedItem.getMerchant());

        saveExpiredItem(expiredItem);
        expiredItemMap.put(expiredItem.getWitherOutTime(), expiredItem);
    }

    /**
     * Starts a runnable to allow for expiring of
     * items.
     */
    private void expireListings() {
        Bukkit.getScheduler().runTaskTimer(PLUGIN, () -> {
            if(!itemListingMap.isEmpty()) {
                Iterator<Map.Entry<Long, ListedItem>> iterator = new TreeMap<>(itemListingMap).entrySet().iterator();

                iterator.forEachRemaining(entry -> {
                    long expire = entry.getKey();

                    if(expire < System.currentTimeMillis()) {
                        entry.getValue().expire();
                    }
                });
            }
        }, 0L, 100L);
    }

}
