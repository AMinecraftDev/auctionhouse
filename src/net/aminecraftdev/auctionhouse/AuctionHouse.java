package net.aminecraftdev.auctionhouse;

import net.aminecraftdev.auctionhouse.commands.AuctionHouseCmd;
import net.aminecraftdev.auctionhouse.handlers.*;
import net.aminecraftdev.auctionhouse.handlers.data.DatabaseManagement;
import net.aminecraftdev.auctionhouse.handlers.gui.AddItemHandler;
import net.aminecraftdev.auctionhouse.handlers.gui.ChangePriceHandler;
import net.aminecraftdev.auctionhouse.handlers.gui.ExpiredItemsHandler;
import net.aminecraftdev.auctionhouse.handlers.gui.ListHandler;
import net.aminecraftdev.auctionhouse.managers.AuctionDataManager;
import net.aminecraftdev.auctionhouse.utils.FileUtils;
import net.aminecraftdev.auctionhouse.utils.core.Message;
import net.aminecraftdev.auctionhouse.utils.database.AbstractDatabase;
import net.aminecraftdev.auctionhouse.utils.database.mysql.MySQL;
import net.aminecraftdev.auctionhouse.utils.database.sqlite.SQLite;
import net.aminecraftdev.auctionhouse.utils.dependencies.VaultHelper;
import net.aminecraftdev.auctionhouse.utils.inventory.Panel;
import net.aminecraftdev.auctionhouse.utils.reflection.ReflectionUtils;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.sql.SQLException;
import java.util.logging.Level;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 20-Sep-17
 */
public class AuctionHouse extends JavaPlugin {

    private static AuctionHouse i;
    private static AbstractDatabase abstractDatabase;
    private static AuctionDataManager auctionDataManager;

    private DataManagement dataManagement;
    private File inventoriesF, langF, configF;
    private FileConfiguration inventoriesC, langC, configC;
    private boolean useMySQL, useSQLite;

    public void onEnable() {
        i = this;
        Panel.setPlugin(this);
        dataManagement = new DatabaseManagement(this);
        new ReflectionUtils();

        intializeFiles();

        if(!VaultHelper.setupVault()) {
            getLogger().log(Level.WARNING, "The plugin cannot load because no Vault plugin(s) were found.");
            getLogger().log(Level.WARNING, "Please install Vault and any other required plugins.");
            setEnabled(false);
            return;
        }

        if(!initializeDataManagement()) {
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }

        dataManagement.createTables();

        initializeManagers();
        initializeHandlers();

        new AuctionHouseCmd();
    }

    public void onDisable() {
        saveFiles();
    }

    private void intializeFiles() {
        this.inventoriesF = new File(getDataFolder(), "inventories.yml");
        this.langF = new File(getDataFolder(), "lang.yml");
        this.configF = new File(getDataFolder(), "config.yml");

        if(!this.configF.exists()) saveResource("config.yml", false);
        if(!inventoriesF.exists()) saveResource("inventories.yml", false);
        if(!langF.exists()) saveResource("lang.yml", false);

        reloadFiles();

        for(Message message : Message.values()) {
            if(!this.langC.isSet(message.getPath())) {
                langC.set(message.getPath(), message.getDefault());
            }
        }

        saveFiles();
        Message.setFile(this.langC);

        this.useSQLite = getConfig().getBoolean("Database.SQLite.enabled");
        this.useMySQL = getConfig().getBoolean("Database.MySQL.enabled");
    }

    /**
     * Initializes the connection between
     * the specified database or not.
     *
     * @return if it was successful or not
     */
    private boolean initializeDataManagement() {
        if(useSQLite) {
            abstractDatabase = new SQLite(this);
        } else if(useMySQL) {
            String host = getConfig().getString("Database.MySQL.host");
            String port = getConfig().getString("Database.MySQL.port");
            String database = getConfig().getString("Database.MySQL.database");
            String username = getConfig().getString("Database.MySQL.username");
            String password = getConfig().getString("Database.MySQL.password");

            abstractDatabase = new MySQL(this, host, port, database, username, password);
        }

        try {
            abstractDatabase.openConnection();
            return true;
        } catch (SQLException | ClassNotFoundException e) {
            getLogger().log(Level.WARNING, "Database failed to open the connection, therefore plugin Disabled.");
            return false;
        }
    }

    /**
     * Initialize the managers for this plugin
     */
    private void initializeManagers() {
        auctionDataManager = new AuctionDataManager(dataManagement);
    }

    /**
     * Initialize any handles of this plugin
     */
    private void initializeHandlers() {
        AuctionPlayer.load(auctionDataManager);
        ExpiredItem.load(auctionDataManager);
        GUIHandler.load(auctionDataManager);
        ListedItem.load(auctionDataManager);

        ExpiredItemsHandler.load();
        ChangePriceHandler.load();
        AddItemHandler.load();
        ListHandler.load();
        ItemListing.load();
    }

    public FileConfiguration getInventories() {
        return inventoriesC;
    }

    public FileConfiguration getConfig() {
        return configC;
    }

    public FileConfiguration getLang() {
        return langC;
    }

    public void saveFiles() {
        FileUtils.saveFile(this.langF, this.langC);
        FileUtils.saveFile(this.configF, this.configC);
        FileUtils.saveFile(this.inventoriesF, this.inventoriesC);
    }

    public void reloadFiles() {
        this.inventoriesC = FileUtils.getConf(this.inventoriesF);
        this.langC = FileUtils.getConf(this.langF);
        this.configC = FileUtils.getConf(this.configF);
    }

    public AbstractDatabase getAbstractDatabase() {
        return abstractDatabase;
    }

    public static AuctionHouse getI() {
        return i;
    }

}
