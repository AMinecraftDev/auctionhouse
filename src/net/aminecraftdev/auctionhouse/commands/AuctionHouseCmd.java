package net.aminecraftdev.auctionhouse.commands;

import net.aminecraftdev.auctionhouse.handlers.gui.ListHandler;
import net.aminecraftdev.auctionhouse.utils.command.builder.CommandBuilder;
import net.aminecraftdev.auctionhouse.utils.command.builder.CommandHandler;
import net.aminecraftdev.auctionhouse.utils.core.Message;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 20-Sep-17
 */
public class AuctionHouseCmd implements CommandHandler {

    public AuctionHouseCmd() {
        new CommandBuilder(this)
                .setCmdName("auctionhouse")
                .setCmdDescription("The default command for the AuctionHouse plugin.")
                .setCmdAliases("ah", "auc", "auch")
                .setUsage("/<command>")
                .setPlayerOnly(true)
                .setUseArgsHelp(false)
                .setCmdPermission("auctionhouse.use")
                .setMustBePlayerMessage(Arrays.asList(Message.MUST_BE_PLAYER.toString().split("\n")))
                .setNoPermissionMessage(Arrays.asList(Message.NO_PERMISSION.toString().split("\n")))
                .register();
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        Player player = (Player) commandSender;

        new ListHandler(player).open();
    }
}
