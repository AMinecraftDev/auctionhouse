package net.aminecraftdev.auctionhouse.utils.itemstack.enchants;

import net.aminecraftdev.auctionhouse.utils.factory.NbtFactory;
import org.bukkit.inventory.ItemStack;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 08-Jun-17
 */
public class GlowEnchant {

    public static final ItemStack addGlow(ItemStack base) {
        ItemStack craftStack = NbtFactory.getCraftItemStack(base);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);
        NbtFactory.NbtList newList = NbtFactory.createList();

        compound.put("ench", newList);
        return craftStack;
    }

}
