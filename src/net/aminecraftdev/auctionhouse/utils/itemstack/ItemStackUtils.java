package net.aminecraftdev.auctionhouse.utils.itemstack;

import net.aminecraftdev.auctionhouse.utils.NumberUtils;
import net.aminecraftdev.auctionhouse.utils.factory.NbtFactory;
import net.aminecraftdev.auctionhouse.utils.itemstack.enchants.GlowEnchant;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by charl on 28-Apr-17.
 */
public class ItemStackUtils {

    public static ItemStack createItemStack(ItemStack itemStack, Map<String,String> replaceMap) {
        return createItemStack(itemStack, replaceMap, null);
    }

    public static ItemStack createItemStack(ItemStack itemStack, Map<String,String> replaceMap, Map<String,Object> compoundData) {
        ItemStack cloneStack = itemStack.clone();
        ItemMeta itemMeta = cloneStack.getItemMeta();
        boolean hasName = cloneStack.getItemMeta().hasDisplayName();
        boolean hasLore = cloneStack.getItemMeta().hasLore();
        String name = "";
        List<String> newLore = new ArrayList<>();

        if(hasName) name = cloneStack.getItemMeta().getDisplayName();

        if(replaceMap != null && !replaceMap.isEmpty()) {
            for(String s : replaceMap.keySet()) {
                if(hasName) {
                    name = name.replace(s, replaceMap.get(s));
                }
            }

            if(hasLore) {
                for(String s : itemMeta.getLore()) {
                    for(String z : replaceMap.keySet()) {
                        if(s.contains(z)) s = s.replace(z, replaceMap.get(z));
                    }

                    newLore.add(s);
                }
            }
        }

        if(hasLore) itemMeta.setLore(newLore);
        if(hasName) itemMeta.setDisplayName(name);

        cloneStack.setItemMeta(itemMeta);

        if(compoundData == null || compoundData.isEmpty()) return cloneStack;

        ItemStack craftStack = NbtFactory.getCraftItemStack(cloneStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        for(String s : compoundData.keySet()) {
            compound.put(s, compoundData.get(s));
        }

        return craftStack;
    }

    public static ItemStack createItemStack(ConfigurationSection configurationSection) {
        return createItemStack(configurationSection, 1, null);
    }

    public static ItemStack createItemStack(ConfigurationSection configurationSection, int amount, Map<String, String> replacedMap) {
        String type = configurationSection.getString("type");
        String name = configurationSection.getString("name");
        List<String> lore = configurationSection.getStringList("lore");
        List<String> enchants = configurationSection.getStringList("enchants");
        short durability = (short) configurationSection.getInt("durability");
        String owner = configurationSection.getString("owner");
        Map<Enchantment, Integer> map = new HashMap<>();
        List<String> newLore = new ArrayList<>();
        Material mat = getType(type);
        short meta = 0;
        boolean addGlow = false;

        if(type instanceof String) {
            String sType = (String) type;

            if(sType.contains(":")) {
                String[] split = sType.split(":");

                meta = Short.valueOf(split[1]);
            }
        }

        if((replacedMap != null) && (name != null)) {
            for(String z : replacedMap.keySet()) {
                if(!name.contains(z)) continue;

                name = name.replace(z, replacedMap.get(z));
            }
        }

        if(lore != null) {
            for(String z : lore) {
                String y = z;

                if(replacedMap != null) {
                    for(String x : replacedMap.keySet()) {
                        if(!y.contains(x)) continue;

                        y = y.replace(x, replacedMap.get(x));
                    }
                }

                if(y.contains("\n")) {
                    String[] split = y.split("\n");

                    for(String s2 : split) {
                        newLore.add(ChatColor.translateAlternateColorCodes('&', s2));
                    }
                } else {
                    newLore.add(ChatColor.translateAlternateColorCodes('&', y));
                }
            }
        }

        if(enchants != null) {
            for(String s : enchants) {
                String[] spl = s.split(":");
                String ench = spl[0];
                int level = Integer.parseInt(spl[1]);

                if(ench.equalsIgnoreCase("SWEEPING_EDGE")) {
                    map.put(Enchantment.SWEEPING_EDGE, level);
                } else if(ench.equalsIgnoreCase("GLOW")) {
                    addGlow = true;
                } else {
                    map.put(Enchantment.getByName(ench), level);
                }
            }
        }

        ItemStack itemStack = new ItemStack(mat, amount, meta);
        ItemMeta itemMeta = itemStack.getItemMeta();

        if(!newLore.isEmpty()) {
            itemMeta.setLore(newLore);
        }
        if(name != null) {
            if(!name.equals("")) {
                itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
            }
        }

        itemStack.setItemMeta(itemMeta);

        if(!map.isEmpty()) {
            itemStack.addUnsafeEnchantments(map);
        }
        if(configurationSection.contains("durability")) {
            short dura = itemStack.getType().getMaxDurability();
            dura -= (short) durability - 1;

            itemStack.setDurability(dura);
        }

        if(configurationSection.contains("owner") && itemStack.getType() == Material.SKULL_ITEM) {
            SkullMeta skullMeta = (SkullMeta) itemStack.getItemMeta();

            skullMeta.setOwner(owner);

            itemStack.setItemMeta(skullMeta);
        }

        if(addGlow) return addGlow(itemStack);
        return itemStack;
    }

    public static Material getType(String string) {
        Material material = Material.getMaterial(string);

        if(material == null) {
            if(NumberUtils.isStringInteger(string)) {
                material = Material.getMaterial(Integer.valueOf(string));

                if(material != null) return material;

                return null;
            } else {
                String[] split = string.split(":");

                material = Material.getMaterial(Integer.valueOf(split[0]));

                if(material != null) return material;

                return null;
            }
        }

        return material;
    }

    public static ItemStack addGlow(ItemStack itemStack) {
        return GlowEnchant.addGlow(itemStack);
    }
}