package net.aminecraftdev.auctionhouse.utils;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 24-Sep-17
 */
public interface Callback<T> {

    void call(T object);

}
