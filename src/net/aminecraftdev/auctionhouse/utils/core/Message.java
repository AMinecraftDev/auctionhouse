package net.aminecraftdev.auctionhouse.utils.core;

import net.aminecraftdev.auctionhouse.utils.message.MessageUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.text.SimpleDateFormat;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 24-May-17
 */
public enum Message {

    DATE_FORMAT("GeneralErrors.date-format", "MM-d-yyyy hh:mm:ss"), // 3/31/15 07:49AM
    NO_PERMISSION("GeneralErrors.NoPermission", "&c&l(!) &cYou do not have permission to use that."),
    MUST_BE_PLAYER("GeneralErrors.MustBePlayer", "&c&l(!) &cYou must be a player to use that."),
    INVALID_INTEGER("GeneralErrors.InvalidNumber", "&c&l(!) &cYou specified an invalid number, try something like 1 or 5000."),
    INVALID_DOUBLE("GeneralErrors.InvalidDouble", "&c&l(!) &cYou specified an invalid double, try something like 1.4 or 2.3."),
    INVENTORY_SPACE("GeneralErrors.InventorySpace", "&c&l(!) &cYou do not have enough inventory space."),
    NOT_ONLINE("GeneralErrors.NotOnline", "&c&l(!) &cThe specified player is offline."),
    ITEMSTACK_MIN("GeneralErrors.MinItemStack", "&c&l(!) &cThe minimum ItemStack size is 1."),
    ITEMSTACK_MAX("GeneralErrors.MaxItemStack", "&c&l(!) &cThe maximum ItemStack size is 64."),
    ITEMSTACK_NULL("GeneralErrors.NullItemStack", "&c&l(!) &cThe specified ItemStack cannot be null or air."),
    INSUFFICIENT_FUNDS("GeneralErrors.InsufficientFunds", "&c&l(!) &cYou have insufficient funds for that transaction!"),
    MONEY_TAKEN("GeneralErrors.MoneyTaken", "&c&l- ${0}"),
    MONEY_GIVEN("GeneralErrors.MoneyGiven", "&a&l+ ${0}"),

    AddItem_BelowMinListingHours("&c&l(!) &cYou cannot reduce the listing time for this item any lower."),
    AddItem_CannotListNull("&c&l(!) &cYou cannot list null or air!"),
    AddItem_ExceedsMaxListingHours("&c&l(!) &cYou cannot list the item for any longer amount of hours."),
    AddItem_Listed("&e&l(!) &fYour item has been listed for &a$&7{0}&f for {1} hour(s)."),
    AddItem_NotEnoughMoney("&c&l(!) &cYou do not have enough money to afford that listing!"),
    AddItem_TooManyListed("&c&l(!) &cYou cannot list another item because you would exceed your limit."),

    AdminRemoveListedItem("&e&l(!) &eYou have removed the listed item from auction."),

    NoLongerForSale("&c&l(!) &cThat item is no longer for sale!"),

    OwnItem("&c&l(!) &cYou cannot buy your own item."),

    PlayerBoughtListedItemSender("&e&l(!) &fYou have bought the listed item for &a$&f{0}&f."),
    PlayerBoughtListedItemReceiver("&e&l(!) &fSomeone has bought your listed item for &a$&f{0}&f."),
    PlayerRemoveListedItem("&e&l(!) &eYou have removed the listed item and it has been returned to your expired box.");

    private String path;
    private String msg;
    private static FileConfiguration LANG;
    public static SimpleDateFormat sdf;

    Message(String path, String start) {
        this.path = path;
        this.msg = start;
    }

    Message(String string) {
        this.path = this.name().replace("_", ".");
        this.msg = string;
    }

    public static void setFile(FileConfiguration configuration) {
        LANG = configuration;
        sdf = new SimpleDateFormat(DATE_FORMAT.toString());
    }

    @Override
    public String toString() {
        String s = ChatColor.translateAlternateColorCodes('&', LANG.getString(this.path, msg));

        return s;
    }

    public String toString(Object... args) {
        String s = ChatColor.translateAlternateColorCodes('&', LANG.getString(this.path, msg));

        return getFinalized(s, args);
    }

    public String getDefault() {
        return this.msg;
    }

    public String getPath() {
        return this.path;
    }

    public void msg(CommandSender p, Object... order) {
        String s = toString();

        if(s.contains("\n")) {
            String[] split = s.split("\n");

            for(String inner : split) {
                sendMessage(p, inner, order);
            }
        } else {
            sendMessage(p, s, order);
        }
    }

    public void broadcast(Object... order) {
        String s = toString();

        if(s.contains("\n")) {
            String[] split = s.split("\n");

            for(String s1 : split) {
                for(Player player : Bukkit.getOnlinePlayers()) {
                    sendMessage(player, s1, order);
                }
            }
        } else {
            for(Player player : Bukkit.getOnlinePlayers()) {
                sendMessage(player, s, order);
            }
        }
    }

    /**
     *
     * Private Enum methods
     *
     *
     */

    private String getFinalized(String string, Object... order) {
        int current = 0;

        for(Object object : order) {
            String placeholder = "{" + current + "}";

            if(string.contains(placeholder)) {
                if(object instanceof CommandSender) {
                    string = string.replace(placeholder, ((CommandSender) object).getName());
                }
                else if(object instanceof OfflinePlayer) {
                    string = string.replace(placeholder, ((OfflinePlayer) object).getName());
                }
                else if(object instanceof Location) {
                    Location location = (Location) object;
                    String repl = location.getWorld().getName() + ", " + location.getBlockX() + ", " + location.getBlockY() + ", " + location.getBlockZ();

                    string = string.replace(placeholder, repl);
                }
                else if(object instanceof String) {
                    string = string.replace(placeholder, MessageUtils.translateString((String) object));
                }
                else if(object instanceof Double) {
                    string = string.replace(placeholder, ""+object);
                }
                else if(object instanceof Integer) {
                    string = string.replace(placeholder, ""+object);
                }
                else if(object instanceof ItemStack) {
                    string = string.replace(placeholder, getItemStackName((ItemStack) object));
                }
            }

            current++;
        }

        return string;
    }

    private void sendMessage(CommandSender target, String string, Object... order) {
        string = getFinalized(string, order);

        if(target instanceof Player) {
            if(string.contains("{c}")) {
                MessageUtils.sendCenteredMessage(target, string);
            } else {
                target.sendMessage(string);
            }
        } else {
            if(string.contains("{c}")) {
                MessageUtils.sendCenteredMessage(target, string);
            } else {
                target.sendMessage(string);
            }
        }
    }

    private String getItemStackName(ItemStack itemStack) {
        String name = itemStack.getType().toString().replace("_", " ");

        if(itemStack.hasItemMeta() && itemStack.getItemMeta().hasDisplayName()) {
            return itemStack.getItemMeta().getDisplayName();
        }

        return name;
    }

}
