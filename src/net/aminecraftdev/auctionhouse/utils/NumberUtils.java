package net.aminecraftdev.auctionhouse.utils;

import java.text.DecimalFormat;

/**
 * Created by charl on 28-Apr-17.
 */
public class NumberUtils {

    public static final String formatDouble(double d) {
        DecimalFormat format = new DecimalFormat("###,###,###,###,###.##");

        return format.format(d);
    }

    public static final boolean isStringInteger(String s) {
        try {
            Integer.valueOf(s);
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }

    public static final boolean isStringDouble(String s) {
        try {
            Double.valueOf(s);
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }

}