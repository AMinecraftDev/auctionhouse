package net.aminecraftdev.auctionhouse.utils;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

/**
 * Created by charl on 07-May-17.
 */
public class FileUtils {

    public static final void saveFile(File file, FileConfiguration configuration) {
        try {
            configuration.save(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static final FileConfiguration getConf(File file) {
        return YamlConfiguration.loadConfiguration(file);
    }

}
