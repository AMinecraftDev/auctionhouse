package net.aminecraftdev.auctionhouse.utils.database.mysql;

import net.aminecraftdev.auctionhouse.utils.database.AbstractDatabase;
import org.bukkit.plugin.Plugin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 18-Jul-17
 */
public class MySQL extends AbstractDatabase {

    private final String user, database, password, port, hostname;

    public MySQL(Plugin plugin, String hostname, String port, String database, String username, String password) {
        super(plugin);

        this.hostname = hostname;
        this.port = port;
        this.database = database;
        this.user = username;
        this.password = password;
    }

    @Override
    public Connection openConnection()
            throws SQLException, ClassNotFoundException {
        if(checkConnection()) return this.connection;

        Class.forName("com.mysql.jdbc.Driver");

        Properties properties = new Properties();

        properties.setProperty("user", this.user);
        properties.setProperty("password", this.password);
        properties.setProperty("useSSL", "false");

        this.connection = DriverManager.getConnection("jdbc:mysql://" + this.hostname + ":" + this.port + "/" + this.database, properties);

        return this.connection;
    }
}
