package net.aminecraftdev.auctionhouse.utils.database.sqlite;

import net.aminecraftdev.auctionhouse.utils.database.AbstractDatabase;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Queue;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 20-Sep-17
 */
public class SQLite extends AbstractDatabase {

    private File databaseFile;

    public SQLite(Plugin plugin) {
        super(plugin);
    }

    @Override
    public Connection openConnection()
            throws SQLException, ClassNotFoundException {
        if(checkConnection()) return this.connection;

        Class.forName("org.sqlite.JDBC");

        databaseFile = new File(plugin.getDataFolder(), "database.db");

        if(!databaseFile.exists()) {
            try {
                databaseFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        this.connection = DriverManager.getConnection("jdbc:sqlite:" + databaseFile);
        return this.connection;
    }
}
