package net.aminecraftdev.auctionhouse.utils.database;

import net.aminecraftdev.auctionhouse.utils.Callback;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Queue;
import java.util.logging.Level;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 18-Jul-17
 */
public abstract class AbstractDatabase {

    protected Connection connection;
    protected Plugin plugin;

    protected AbstractDatabase(Plugin plugin) {
        this.plugin = plugin;
        this.connection = null;
    }

    public abstract Connection openConnection()
            throws SQLException, ClassNotFoundException;

    public boolean checkConnection()
            throws SQLException {
        return (this.connection != null) && (!this.connection.isClosed());
    }

    public void createTable(String table, Queue<String> queue, List<String> defaultKeys) {
        try {
            if(!checkConnection()) openConnection();
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
            return;
        }

        String createKeys = "";
        int size = defaultKeys.size();
        int current = 0;

        for(String s : defaultKeys) {
            createKeys += s;
            current++;

            if(current < size) {
                createKeys += ", ";
            }
        }

        final String create = "CREATE TABLE IF NOT EXISTS " + table + " (" + createKeys + ")";
        String key = "";
        String value = "";

        current = 0;

        if(queue != null && !queue.isEmpty()) {
            while(!queue.isEmpty()) {
                String string = queue.poll();
                String[] split = string.split("~");
                String k = split[0];
                String v = split[1];

                key += k;
                value += v;

                current++;

                if(!queue.isEmpty()) {
                    key += ", ";
                    value += ", ";
                }
            }
        }

        final String check = "INSERT INTO " + table + " (" + key + ") VALUES (" + value + ")";

        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try {
                executeSQL(create);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        if(queue == null) return;

        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try {
                updateSQL(check);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
    }

    public Connection getConnection() {
        return this.connection;
    }

    public boolean closeConnection()
            throws SQLException {
        if(this.connection == null) return false;

        this.connection.close();
        return true;
    }

    public ResultSet querySQL(String query) throws SQLException, ClassNotFoundException {
        if(!checkConnection()) openConnection();

        Statement statement = this.connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        statement.close();
        return resultSet;
    }

    public int updateSQL(String query) throws SQLException, ClassNotFoundException {
        if(!checkConnection()) openConnection();

        Statement statement = this.connection.createStatement();
        int returnValue = statement.executeUpdate(query);

        statement.close();
        return returnValue;
    }

    public boolean executeSQL(String query) throws SQLException, ClassNotFoundException {
        if(!checkConnection()) openConnection();

        Statement statement = this.connection.createStatement();
        boolean executed = statement.execute(query);

        statement.close();
        return executed;
    }
}
