package net.aminecraftdev.auctionhouse.utils.command;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 26-Jun-17
 */
public interface SubCommandHandler {

    void addSubCommand(SubCommand subCommand);

}
