package net.aminecraftdev.auctionhouse.utils.command.builder;

import net.aminecraftdev.auctionhouse.utils.message.MessageUtils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 26-Sep-17
 */
public class CommandBuilder {

    private List<CommandHandler> commandHandlerList = new ArrayList<>();
    private List<String> noPermissionMessage = new ArrayList<>();
    private List<String> mustBePlayerMessage = new ArrayList<>();
    private List<String> argsHelpMessage = new ArrayList<>();
    private List<String> arguments = new ArrayList<>();
    private boolean playerOnly = false, argsHelp = false;
    private String name, desc, permission, usage;

    public CommandBuilder(CommandHandler... commandHandlers) {
        commandHandlerList.addAll(Arrays.asList(commandHandlers));
    }

    public CommandBuilder setCmdName(String name) {
        this.name = name;
        return this;
    }

    protected String getCmdName() {
        return name;
    }

    public CommandBuilder setCmdDescription(String description) {
        this.desc = description;
        return this;
    }

    protected String getCmdDescription() {
        return desc;
    }

    public CommandBuilder setCmdAliases(String... aliases) {
        this.arguments.clear();
        this.arguments.addAll(Arrays.asList(aliases));
        return this;
    }

    protected List<String> getAliases() {
        return arguments;
    }

    public CommandBuilder setCmdPermission(String permission) {
        this.permission = permission;
        return this;
    }

    protected String getCmdPermission() {
        return permission;
    }

    public CommandBuilder setPlayerOnly(boolean bool) {
        this.playerOnly = bool;
        return this;
    }

    protected boolean isPlayerOnly() {
        return playerOnly;
    }

    public CommandBuilder setUseArgsHelp(boolean bool) {
        this.argsHelp = bool;
        return this;
    }

    protected boolean useArgsHelp() {
        return argsHelp;
    }

    public CommandBuilder setArgsHelpMessage(List<String> argsHelpMessage) {
        this.argsHelpMessage.clear();
        this.argsHelpMessage.addAll(argsHelpMessage);
        return this;
    }

    protected List<String> getArgsHelpMessage() {
        return argsHelpMessage;
    }

    public CommandBuilder setNoPermissionMessage(List<String> noPermissionMessage) {
        this.noPermissionMessage.clear();
        this.noPermissionMessage.addAll(noPermissionMessage);
        return this;
    }

    protected List<String> getNoPermissionMessage() {
        return noPermissionMessage;
    }

    public CommandBuilder setMustBePlayerMessage(List<String> mustBePlayerMessage) {
        this.mustBePlayerMessage.clear();
        this.mustBePlayerMessage.addAll(mustBePlayerMessage);
        return this;
    }

    protected List<String> getMustBePlayerMessage() {
        return mustBePlayerMessage;
    }

    public CommandBuilder setUsage(String usage) {
        this.usage = usage;
        return this;
    }

    protected String getUsage() {
        return usage;
    }

    public void register() {
        new CommandRegister(this);
    }

    protected boolean execute(CommandSender commandSender, String[] args) {
        if(isPlayerOnly() && !(commandSender instanceof Player)) {
            getMustBePlayerMessage().forEach(string -> commandSender.sendMessage(MessageUtils.translateString(string)));
            return false;
        }

        if(getCmdPermission() != null && !commandSender.hasPermission(getCmdPermission())) {
            getNoPermissionMessage().forEach(string -> commandSender.sendMessage(MessageUtils.translateString(string)));
            return false;
        }

        if(useArgsHelp() && args.length == 0) {
            getArgsHelpMessage().forEach(string -> commandSender.sendMessage(MessageUtils.translateString(string)));
            return true;
        }

        commandHandlerList.forEach(commandHandler -> commandHandler.execute(commandSender, args));
        return true;
    }

}
