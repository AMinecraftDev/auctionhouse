package net.aminecraftdev.auctionhouse.utils.command.builder;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;

import java.lang.reflect.Field;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 26-Sep-17
 */
public class CommandRegister extends BukkitCommand {

    private static CommandMap commandMap = null;

    private CommandBuilder commandBuilder;

    protected CommandRegister(CommandBuilder commandBuilder) {
        super(commandBuilder.getCmdName(), commandBuilder.getCmdDescription(), commandBuilder.getUsage(), commandBuilder.getAliases());

        this.commandBuilder = commandBuilder;
        register();
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        return commandBuilder.execute(commandSender, strings);
    }

    private void register() {
        if(commandMap != null) {
            commandMap.register(commandBuilder.getCmdName(), this);
        }

        try {
            Field field = Bukkit.getServer().getClass().getDeclaredField("commandMap");

            field.setAccessible(true);

            commandMap = (CommandMap) field.get(Bukkit.getServer());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        commandMap.register(commandBuilder.getCmdName(), this);
    }
}
