package net.aminecraftdev.auctionhouse.utils.command.builder;

import org.bukkit.command.CommandSender;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 26-Sep-17
 */
public interface CommandHandler {

    void execute(CommandSender commandSender, String[] args);

}
