package net.aminecraftdev.auctionhouse.utils.inventory;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.Inventory;

import java.util.Map;
import java.util.Set;

/**
 * Created by charl on 28-Apr-17.
 */
public interface IInventoryBuilder {

    Panel getInventory();

    Inventory getClonedInventory();

    Inventory build();

    void addReplacedMap(Map<String, String> map);

    ConfigurationSection getConfigurationSection();

    ConfigurationSection getItemConfigSection();

    Set<Integer> getSetOfItemSlots();

}
