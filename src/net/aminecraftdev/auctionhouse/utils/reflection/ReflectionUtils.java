package net.aminecraftdev.auctionhouse.utils.reflection;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;

/**
 * Created by charl on 28-Apr-17.
 */
public class ReflectionUtils {

    private static double nmsDoubleVersion = -1.0;
    private static String nmsVersion = null;

    public ReflectionUtils() {
        if(nmsVersion == null) {
            nmsVersion = Bukkit.getServer().getClass().getPackage().getName();
            nmsVersion = nmsVersion.substring(nmsVersion.lastIndexOf(".") + 1);
        }

        if(nmsDoubleVersion == -1) {
            String str1 = Bukkit.getServer().getClass().getPackage().getName();
            String[] array = str1.replace(".", ",").split(",")[3].split("_");
            String str2 = array[0].replace("v", "");
            String str3 = array[1];

            nmsDoubleVersion = Double.parseDouble(str2 + "." + str3);
        }
    }

    public static String getAPIVersion() {
        return nmsVersion;
    }

    public static Class<?> getNMSClass(String path) {
        try {
            return Class.forName("net.minecraft.server." + getAPIVersion() + "." + path);
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    public static Class<?> getOBCClass(String path) {
        try {
            return Class.forName("org.bukkit.craftbukkit." + getAPIVersion() + "." + path);
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    protected static double getDoubleNMSVersion() {
        return nmsDoubleVersion;
    }
}
