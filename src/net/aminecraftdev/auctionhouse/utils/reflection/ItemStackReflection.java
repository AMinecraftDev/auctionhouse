package net.aminecraftdev.auctionhouse.utils.reflection;

import org.apache.commons.codec.binary.Base64;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 14-Jul-17
 */
public class ItemStackReflection extends ReflectionUtils {

    private static Class<?> craftItemStackClazz, nmsItemStackClazz, nmsItemClazz, localeClazz, nbtTagCompound, nbtCompressedStreamTools;
    private static Method nmsCopy, save, aOut, aIn, createStack, asBukkitCopy;

    public static String serialize(ItemStack itemStack) {
        if(itemStack == null || itemStack.getType() == Material.AIR) return "null";

        ByteArrayOutputStream byteArrayOutputStream = null;

        try {
            if(nbtTagCompound == null) nbtTagCompound = getNMSClass("NBTTagCompound");
            if(craftItemStackClazz == null) craftItemStackClazz = getOBCClass("inventory.CraftItemStack");
            if(nmsItemStackClazz == null) nmsItemStackClazz = getNMSClass("ItemStack");
            if(nbtCompressedStreamTools == null) nbtCompressedStreamTools = getNMSClass("NBTCompressedStreamTools");

            if(nmsCopy == null) nmsCopy = craftItemStackClazz.getMethod("asNMSCopy", ItemStack.class);
            if(save == null) save = nmsItemStackClazz.getMethod("save", nbtTagCompound);
            if(aOut == null) aOut = nbtCompressedStreamTools.getMethod("a", nbtTagCompound, OutputStream.class);

            Object newNbtTagCompoundInstance = nbtTagCompound.getConstructor().newInstance();
            Object obcItemStack = nmsCopy.invoke(null, itemStack);

            save.invoke(obcItemStack, newNbtTagCompoundInstance);
            byteArrayOutputStream = new ByteArrayOutputStream();
            aOut.invoke(null, newNbtTagCompoundInstance, byteArrayOutputStream);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return Base64.encodeBase64String(byteArrayOutputStream.toByteArray());
    }

    public static ItemStack deserializeItemStack(String data) {
        if(data == null) return null;

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(Base64.decodeBase64(data));

        try {
            if(nbtTagCompound == null) nbtTagCompound = getNMSClass("NBTTagCompound");
            if(nmsItemStackClazz == null) nmsItemStackClazz = getNMSClass("ItemStack");
            if(craftItemStackClazz == null) craftItemStackClazz = getOBCClass("inventory.CraftItemStack");
            if(nbtCompressedStreamTools == null) nbtCompressedStreamTools = getNMSClass("NBTCompressedStreamTools");

            if(aIn == null) aIn = nbtCompressedStreamTools.getMethod("a", InputStream.class);
            if(asBukkitCopy == null) asBukkitCopy = craftItemStackClazz.getMethod("asBukkitCopy", nmsItemStackClazz);

            Object compressedStream = aIn.invoke(null, byteArrayInputStream);
            Object itemStackObject;

            if(getDoubleNMSVersion() == 1.11 || getDoubleNMSVersion() == 1.12 || getDoubleNMSVersion() == 1.13 || getDoubleNMSVersion() == 1.14) {
                itemStackObject = nmsItemStackClazz.getConstructor(nbtTagCompound).newInstance(compressedStream);
            } else {
                if(createStack == null) createStack = nmsItemStackClazz.getMethod("createStack", nbtTagCompound);

                itemStackObject = createStack.invoke(null, compressedStream);
            }

            return (ItemStack) asBukkitCopy.invoke(null, itemStackObject);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public static String getFriendlyName(Material material) {
        return material == null ? "Air" : getFriendlyName(new ItemStack(material), false);
    }

    public static String getFriendlyName(ItemStack itemStack, boolean checkDisplayName) {
        if(itemStack == null || itemStack.getType() == Material.AIR) return "Air";

        try {
            if(craftItemStackClazz == null) craftItemStackClazz = getOBCClass("inventory.CraftItemStack");
            if(nmsItemStackClazz == null) nmsItemStackClazz = getNMSClass("ItemStack");
            if(nmsItemClazz == null) nmsItemClazz = getNMSClass("Item");
            if(localeClazz == null) localeClazz = getNMSClass("LocaleI18n");
            if(nmsCopy == null) nmsCopy = craftItemStackClazz.getMethod("asNMSCopy", ItemStack.class);

            Object nmsItemStack = nmsCopy.invoke(null, itemStack);
            Object itemName;

            if(checkDisplayName) {
                Method getName = nmsItemStackClazz.getMethod("getName");

                itemName = getName.invoke(nmsItemStack);
            } else {
                Method getItem = nmsItemStackClazz.getMethod("getItem");
                Object nmsItem = getItem.invoke(nmsItemStack);
                Method getName = nmsItemClazz.getMethod("getName");
                Object localItemName = getName.invoke(nmsItem);
                Method getLocale = localeClazz.getMethod("get", String.class);
                Object localeString = localItemName == null? "" : getLocale.invoke(null, localItemName);

                itemName = ("" + getLocale.invoke(null, localeString.toString() + ".name")).trim();
            }

            return itemName != null? itemName.toString() : capitalizeFully(itemStack.getType().name().replace("_", " ").toLowerCase());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return capitalizeFully(itemStack.getType().name().replace("_", " ").toLowerCase());
    }

    public static String capitalizeFully(String name) {
        if (name != null) {
            if (name.length() > 1) {
                if (name.contains("_")) {
                    StringBuilder sbName = new StringBuilder();
                    for (String subName : name.split("_"))
                        sbName.append(subName.substring(0, 1).toUpperCase() + subName.substring(1).toLowerCase()).append(" ");
                    return sbName.toString().substring(0, sbName.length() - 1);
                } else {
                    return name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
                }
            } else {
                return name.toUpperCase();
            }
        } else {
            return "";
        }
    }

}
